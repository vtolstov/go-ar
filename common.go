package ar

import "time"

const (
	blockSize = 60

	ArMag    = "!<arch>\n"
	ArMagLen = 8
	ArfMag   = "`\n"
)

type Header struct {
	Name string    // name of header file entry
	Mode int64     // permission and mode bits
	Uid  int       // user id of owner
	Gid  int       // group id of owner
	Size int64     // length in bytes
	Date time.Time // modified time
}

type slicer []byte

func (sp *slicer) next(n int) (b []byte) {
	s := *sp
	b, *sp = s[0:n], s[n:]
	return
}
