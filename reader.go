package ar

import (
	//	"bytes"
	"errors"
//	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"time"
	"strings"
	"math"
//	"regexp"
)

var (
	ErrHeader = errors.New("archive/ar: invalid ar header")
	ErrMagic  = errors.New("archive/ar: invalid ar file")
)

type Reader struct {
	r            io.Reader
	err          error
	nb           int64 // number of unread bytes for current file entry
	pad          int64 // amount of padding (ignored) after current file entry
	_alreadyRead bool
	_lastEntry bool
}

// NewReader creates a new Reader reading from r.
func NewReader(r io.Reader) *Reader { return &Reader{r: r, _alreadyRead: false, _lastEntry: false} }

// Next advances to the next entry in the ar archive.
func (tr *Reader) Next() (*Header, error) {
	var hdr *Header
	if tr.err == nil {
		tr.skipUnread()
	}
	if tr.err == nil {
		hdr = tr.readHeader()
	}
	return hdr, tr.err
}

// Parse bytes as a NUL-terminated C-style string.
// If a NUL byte is not found then the whole slice is returned as a string.
func cString(b []byte) string {
	n := 0
	for n < len(b) && b[n] != 0 {
		n++
	}
	return strings.TrimSpace(string(b[0:n]))
}

func (tr *Reader) octal(b []byte) int64 {
	// Removing leading spaces.
	for len(b) > 0 && b[0] == ' ' {
		b = b[1:]
	}
	// Removing trailing NULs and spaces.
	for len(b) > 0 && (b[len(b)-1] == ' ' || b[len(b)-1] == '\x00') {
		b = b[0 : len(b)-1]
	}
	x, err := strconv.ParseUint(cString(b), 8, 64)
	if err != nil {
		tr.err = err
	}
	return int64(x)
}

func (tr *Reader) decimal(b []byte) int64 {
	// Removing leading spaces.
	for len(b) > 0 && b[0] == ' ' {
		b = b[1:]
	}
	// Removing trailing NULs and spaces.
	for len(b) > 0 && (b[len(b)-1] == ' ' || b[len(b)-1] == '\x00') {
		b = b[0 : len(b)-1]
	}
	x, err := strconv.ParseUint(cString(b), 10, 64)
	if err != nil {
		tr.err = err
	}
	return int64(x)
}

// Skip any unread bytes in the existing file entry, as well as any alignment padding.
func (tr *Reader) skipUnread() {
	nr := tr.nb + tr.pad // number of bytes to skip
	tr.nb, tr.pad = 0, 0
	if sr, ok := tr.r.(io.Seeker); ok {
		if _, err := sr.Seek(nr, os.SEEK_CUR); err == nil {
			return
		}
	}
	_, tr.err = io.CopyN(ioutil.Discard, tr.r, nr)
}

func (tr *Reader) readHeader() *Header {
	hdr := new(Header)
	var hdrSize int64

	if !tr._alreadyRead {
		hdrSize = blockSize + ArMagLen
	} else {
		hdrSize = blockSize
	}

	header := make([]byte, hdrSize)
	if tr._lastEntry {
		tr.err = io.EOF
		return nil
	}
	if _, tr.err = io.ReadFull(tr.r, header); tr.err != nil {
		return nil
	}

	s := slicer(header)

	if !tr._alreadyRead {
		magic := string(s.next(8))
		if magic != ArMag {
			tr.err = ErrMagic
			return nil
		}
		tr._alreadyRead = true
	}

	hdr.Name = cString(s.next(16))
	if strings.HasSuffix(hdr.Name, "/") {
		hdr.Name = strings.TrimRight(hdr.Name, "/")
	}
	if strings.HasPrefix(hdr.Name, "//") {
		tr.nb = 60
	}
	hdr.Date = time.Unix(tr.decimal(s.next(12)), 0)
	hdr.Uid = int(tr.decimal(s.next(6)))
	hdr.Gid = int(tr.decimal(s.next(6)))
	hdr.Mode = tr.octal(s.next(8))
	hdr.Size = tr.decimal(s.next(10))
	delim := string(s.next(2))
	if delim != ArfMag {
		tr.err = ErrHeader
		return nil
//		fmt.Printf("%s", delim)
//		delim = string(s.next(1))
		//tr._lastEntry = true
	}

	if tr.err != nil {
		tr.err = ErrHeader
		return nil
	}

	tr.nb = int64(hdr.Size)
	tr.pad = -tr.nb & (hdr.Size - 1) // blockSize is a power of two
	if math.Mod(float64(hdr.Size), 2) != 0 {
		tr.pad = 1
	}
	return hdr

}

// Read reads from the current entry in the tar archive.
// It returns 0, io.EOF when it reaches the end of that entry,
// until Next is called to advance to the next entry.
func (tr *Reader) Read(b []byte) (n int, err error) {
	if tr.nb == 0 {
		// file consumed
		return 0, io.EOF
	}

	if int64(len(b)) > tr.nb {
		b = b[0:tr.nb]
	}
	n, err = tr.r.Read(b)
	tr.nb -= int64(n)

	if err == io.EOF && tr.nb > 0 {
		err = io.ErrUnexpectedEOF
	}
	tr.err = err
	return
}
