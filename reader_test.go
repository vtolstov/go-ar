package ar

import (
	"fmt"
	"io"
	"os"
	"testing"
)

func TestAll(t *testing.T) {
	f, err := os.Open("testdata/test.ar")
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	defer f.Close()
	tr := NewReader(f)

	for {
		hdr, e := tr.Next()
		if e == io.EOF {
			break
		}
		if e != nil {
			fmt.Printf("%s\n", e.Error())
			break
		}
		fmt.Printf("%s %d/%d %d %s %s\n", os.FileMode(hdr.Mode).String(), hdr.Uid, hdr.Gid, hdr.Size, hdr.Date.Format("Jan _2 15:04:05 2006"), hdr.Name)
		if hdr.Name == "test.txt" || hdr.Name == "debian-binary" {
			io.Copy(os.Stdout, tr)
		}
	}

}
